using Fritz2GoDaddy.DynDNS.Business;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Models;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.PostConfigure<ApiBehaviorOptions>(options =>
{
    var builtInFactory = options.InvalidModelStateResponseFactory;

    options.InvalidModelStateResponseFactory = context =>
    {
        var loggerFactory = context.HttpContext.RequestServices.GetRequiredService<ILoggerFactory>();
        var logger = loggerFactory.CreateLogger(context.ActionDescriptor.DisplayName ?? "API Call");
        logger.LogWarning("Bad request for {url}", context.HttpContext.Request.GetDisplayUrl());
        return builtInFactory(context);
    };
});

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "Fritz2GoDaddy.DynDNS API",
        Description = "Translates between GET requests from AVM's FritzBox and GoDaddy's REST API",
        License = new OpenApiLicense
        {
            Name = "MIT",
        }
    });
    options.AddSecurityDefinition("http", new()
    {
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.Http,
        Description = "Basic auth",
        Name = "Authorization",
        Scheme = "basic"
    });
    options.AddSecurityRequirement(new OpenApiSecurityRequirement()
    {
        {
            new OpenApiSecurityScheme {
                Reference = new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id = "http"
                },
                Scheme = "basic",
                Name = "basic",
                In = ParameterLocation.Header
            },
            new List<string>()
        }
    });
    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
});
builder.Services.AddAuthentication("http").AddScheme<AuthenticationSchemeOptions, BasicAuthHandler>("http", "Basic Auth", null);
builder.Services.AddAuthorization();
builder.Services.AddHttpClient();
builder.Services.AddTransient<IUpdateService, UpdateService>();
builder.Services.AddTransient<IDNSRecordAPI, GoDaddyAPI>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
