﻿namespace Fritz2GoDaddy.DynDNS.Business
{
    /// <summary>
    /// Implements <see cref="IUpdateService"/>
    /// </summary>
    public class UpdateService : IUpdateService
    {
        private ILogger<UpdateService> Logger { get; }
        private IDNSRecordAPI API { get; }

        /// <summary>
        /// Default constructor for DI
        /// </summary>
        /// <param name="logger">Logging instance</param>
        /// <param name="api">Implementation of a DNS Record API</param>
        public UpdateService(ILogger<UpdateService> logger, IDNSRecordAPI api)
        {
            Logger = logger;
            API = api;
        }

        /// <inheritdoc/>
        public async Task<bool> Update(string ipv4, string ipv6, string domain, string apiKey, string name)
        {
            var results = await Task.WhenAll(UpdateEntryIfNeeded(ipv4, domain, true, apiKey, name), UpdateEntryIfNeeded(ipv6, domain, false, apiKey, name));
            return !results.Any(result => result == false);
        }

        private async Task<bool> UpdateEntryIfNeeded(string value, string domain, bool isIPv4, string apiKey, string name)
        {
            if (string.IsNullOrEmpty(value)) return true;

            var url = isIPv4 ? API.GetIPv4Url(domain, name) : API.GetIPv6Url(domain, name);
            var lastEntry = await API.GetRecord(url, apiKey, name);
            if (lastEntry == value)
                return true;

            Logger.LogInformation("Updating {ipProtocol} entry {name} for {domain} with {value}", isIPv4 ? "IPv4" : "IPv6", name, domain, value);
            return await API.UpdateRecord(url, apiKey, value);
        }
    }
}
