﻿using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Net.Http.Headers;
using System.Text.Json.Serialization;

namespace Fritz2GoDaddy.DynDNS.Business
{
    /// <summary>
    /// Implements <see cref="IDNSRecordAPI"/> for GoDaddy's REST API
    /// </summary>
    public class GoDaddyAPI : IDNSRecordAPI
    {
        private HttpClient HttpClient { get; }
        private ILogger<GoDaddyAPI> Logger { get; }

        /// <summary>
        /// Default constructor for DI
        /// </summary>
        /// <param name="logger">Logging instance</param>
        /// <param name="httpClient">Http Client to perform requests</param>
        public GoDaddyAPI(ILogger<GoDaddyAPI> logger, HttpClient httpClient)
        {
            Logger = logger;
            HttpClient = httpClient;
        }

        /// <inheritdoc/>
        public async Task<string> GetRecord(Uri url, string apiKey, string name)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Authorization = new AuthenticationHeaderValue("sso-key", apiKey);
            request.Headers.Add("Accept", "application/json");

            var result = await HttpClient.SendAsync(request);
            if (result.IsSuccessStatusCode)
            {
                var entries = await result.Content.ReadFromJsonAsync<List<DNSRecord>>();
                return entries?.FirstOrDefault(entry => entry.Name == name)?.Data ?? string.Empty;
            }
            else
            {
                if (result.StatusCode == HttpStatusCode.Unauthorized)
                    Logger.LogWarning("Unauthorized call, is your api key correct?");
                else if (!result.IsSuccessStatusCode)
                    Logger.LogWarning("GET to {url} failed with code {statusCode}", url, result.StatusCode);
                return string.Empty;
            }
        }

        /// <inheritdoc/>
        public async Task<bool> UpdateRecord(Uri url, string apiKey, string value)
        {
            var request = new HttpRequestMessage(HttpMethod.Put, url);
            request.Headers.Authorization = new AuthenticationHeaderValue("sso-key", apiKey);
            request.Content = new StringContent($"[{{\"data\":\"{value}\"}}]", new MediaTypeHeaderValue("application/json"));

            var result = await HttpClient.SendAsync(request);
            if (result.StatusCode == HttpStatusCode.Unauthorized)
                Logger.LogWarning("Unauthorized call, is your api key correct?");
            else if (!result.IsSuccessStatusCode)
                Logger.LogWarning("PUT to {url} failed with code {statusCode}", url, result.StatusCode);
            return result.IsSuccessStatusCode;
        }

        /// <inheritdoc/>
        public Uri GetIPv4Url(string domain, string name) =>
            new($"https://api.godaddy.com/v1/domains/{domain}/records/A/{name}");

        /// <inheritdoc/>
        public Uri GetIPv6Url(string domain, string name) =>
            new($"https://api.godaddy.com/v1/domains/{domain}/records/AAAA/{name}");

        private class DNSRecord
        {
            [JsonPropertyName("data")]
            [Required]
            public required string Data { get; set; }

            [JsonPropertyName("name")]
            [Required]
            public required string Name { get; set; }

            [JsonPropertyName("port")]
            public int? PortSrvOnly { get; set; }

            [JsonPropertyName("priority")]
            public int? PriorityMxSrvOnly { get; set; }

            [JsonPropertyName("protocol")]
            public string? ProtocolSrvOnly { get; set; }

            [JsonPropertyName("service")]
            public string? ServiceSrvOnly { get; set; }

            [JsonPropertyName("ttl")]
            public int? TimeToLive { get; set; }

            [JsonPropertyName("type")]
            [Required]
            public required string Type { get; set; }

            [JsonPropertyName("weight")]
            public int? WeightSrvOnly { get; set; }
        }
    }
}
