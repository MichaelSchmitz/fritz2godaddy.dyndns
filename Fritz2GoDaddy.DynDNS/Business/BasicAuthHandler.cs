﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Text;

namespace Fritz2GoDaddy.DynDNS.Business
{
    /// <summary>
    /// Handler that checks if basic auth was set at all
    /// </summary>
    public class BasicAuthHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        /// <summary>
        /// Default constructor passing params to base class
        /// </summary>
        public BasicAuthHandler(IOptionsMonitor<AuthenticationSchemeOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock) : base(options, logger, encoder, clock)
        {
        }

        /// <inheritdoc />
        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.ContainsKey("Authorization"))
            {
                return Task.FromResult(AuthenticateResult.Fail("Missing Authorization header"));
            }

            var authorizationHeader = Request.Headers["Authorization"].ToString();

            if (!authorizationHeader.StartsWith("Basic ", StringComparison.OrdinalIgnoreCase))
            {
                return Task.FromResult(AuthenticateResult.Fail("Authorization header does not start with 'Basic'"));
            }

            var decoded = Encoding.UTF8.GetString(Convert.FromBase64String(authorizationHeader.Replace("Basic ", "", StringComparison.OrdinalIgnoreCase)));

            var claimsPrincipal = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Name, decoded)
            }, "http"));

            return Task.FromResult(AuthenticateResult.Success(new AuthenticationTicket(claimsPrincipal, Scheme.Name)));
        }
    }

    /// <summary>
    /// Helper extension for ClaimsPrincipal
    /// </summary>
    public static class UserExtensions
    {
        /// <summary>
        /// Extract decoded basic auth credentials for further authentication
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static string GetApiKey(this ClaimsPrincipal user)
        {
            return user.Claims.First(c => c.Type == ClaimTypes.Name).Value;
        }
    }
}
