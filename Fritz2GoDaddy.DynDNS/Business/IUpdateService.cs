﻿namespace Fritz2GoDaddy.DynDNS.Business
{
    /// <summary>
    /// Service to update IP DNS records only if needed
    /// </summary>
    public interface IUpdateService
    {
        /// <summary>
        /// Perform the updates for given IP addressed if they have changed
        /// </summary>
        /// <param name="ipv4">IPv4 IP address</param>
        /// <param name="ipv6">IPv6 IP address</param>
        /// <param name="domain">Domain to set addresses for</param>
        /// <param name="apiKey">API key to use for authentication</param>
        /// <param name="name">Name of the record to update</param>
        /// <returns>True, if all are up to date or updates were successful, false if any failed</returns>
        Task<bool> Update(string ipv4, string ipv6, string domain, string apiKey, string name);
    }
}