﻿namespace Fritz2GoDaddy.DynDNS.Business
{
    /// <summary>
    /// API to query and update DNS records
    /// </summary>
    public interface IDNSRecordAPI
    {
        /// <summary>
        /// Create the URI needed for updating and querying records for IPv4
        /// </summary>
        /// <param name="domain">Domain to query records for</param>
        /// <param name="name">Name of record to query</param>
        /// <returns>URI to use in further calls</returns>
        Uri GetIPv4Url(string domain, string name);

        /// <summary>
        /// Create the URI needed for updating and querying records for IPv6
        /// </summary>
        /// <param name="domain">Domain to query records for</param>
        /// <param name="name">Name of record to query</param>
        /// <returns>URI to use in further calls</returns>
        Uri GetIPv6Url(string domain, string name);

        /// <summary>
        /// Get the value of the record with given name
        /// </summary>
        /// <param name="url">URL to query</param>
        /// <param name="apiKey">API key to authenticate request</param>
        /// <param name="name">Name of record to query</param>
        /// <returns>Value of record, empty if none</returns>
        Task<string> GetRecord(Uri url, string apiKey, string name);

        /// <summary>
        /// Update the value of the record
        /// </summary>
        /// <param name="url">URL to update</param>
        /// <param name="apiKey">API key to authenticate request</param>
        /// <param name="value">Value to update record with</param>
        /// <returns>Success of operation</returns>
        Task<bool> UpdateRecord(Uri url, string apiKey, string value);
    }
}