using Fritz2GoDaddy.DynDNS.Business;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Fritz2GoDaddy.DynDNS.Controllers
{
    /// <summary>
    /// Controller translating GET request to REST APIs with other methods
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class UpdateController : ControllerBase
    {
        private ILogger<UpdateController> Logger { get; }
        private IUpdateService UpdateService { get; }

        /// <summary>
        /// Default constructor for DI
        /// </summary>
        /// <param name="logger">Logging instance</param>
        /// <param name="updateService">Service to perform DNS updates</param>
        public UpdateController(ILogger<UpdateController> logger, IUpdateService updateService)
        {
            Logger = logger;
            UpdateService = updateService;
        }

        /// <summary>
        /// Allows for DynDNS based entirely on a GET request with query parameters
        /// </summary>
        /// <remarks>
        /// Note IP addresses are not validated by this API, your registrar might or might not validate it.
        /// This should nevertheless not be necessary, as this should be called automatically.
        /// 
        /// Any necessary API key is extracted from the Authorization header
        /// </remarks>
        /// <param name="ipv4">IPv4 IP address to set</param>
        /// <param name="ipv6">IPv6 IP address to set</param>
        /// <param name="domain">Domain to set addresses for</param>
        /// <param name="name">Name of the record to update, defaults to @</param>
        [HttpGet]
        public async Task<IActionResult> Get(string? ipv4, string? ipv6, [Required] string domain, string name = "@")
        {
            if (string.IsNullOrEmpty(ipv4) && string.IsNullOrEmpty(ipv6))
            {
                Logger.LogWarning("Neither IPv4 nor IPv6 contained a value, one or both are required");
                return BadRequest("Neither IPv4 nor IPv6 contained a value, one or both are required");
            }
            Logger.LogInformation("Received update request for {domain}", domain);
            return await UpdateService.Update(ipv4 ?? string.Empty, ipv6 ?? string.Empty, domain, User.GetApiKey(), name) ? Ok() : StatusCode(500);
        }


    }
}