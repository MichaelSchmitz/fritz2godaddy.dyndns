# Fritz2GoDaddy.DynDNS
![C#](https://img.shields.io/badge/Language-C%23-blue?style=flat)
![License MIT](https://img.shields.io/gitlab/license/MichaelSchmitz/fritz2godaddy.dyndns?style=flat)

This project provides a wrapper around GoDaddy's REST API to enable AVM's FritzBox DynDNS to work with GoDaddy.
FritzBox's only use GET requests to update IPv4 and IPv6 addresses, GoDaddy requires PUT requests with a body as well as special headers to be set.

Basically this application enables you, to run a docker container locally, which translates between AVM's DynDNS feature and GoDaddy's REST API.

## But why?
You might ask, why not simply use a docker container that directly checks IPs and updates it directly?

Firstly, I could never get it to work reliably with both IPv4 and IPv6 addresses.
But more importantly, if you want to use WireGuard directly with a FritzBox you have to configure DynDNS with it. This simply avoids unnecessary configuration/updates with a provider whose DNS you are not using anyways.

Adaption with other services than GoDaddy's should be relatively straight forward, just reimplement the [IDNSRecordAPI](Fritz2GoDaddy.DynDNS/Business/IDNSRecordAPI.cs) interface for your provider and configure the injection in the [main](Fritz2GoDaddy.DynDNS/Program.cs) file accordingly.


